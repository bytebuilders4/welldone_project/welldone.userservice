﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;
using UserService.Entities.Enums;

namespace UserService.DataAccess.Postgres
{
    public class TrackableEntityInterceptor : SaveChangesInterceptor
    {
        public override ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData, InterceptionResult<int> result, CancellationToken cancellationToken = default)
        {
            var entities = eventData.Context!.ChangeTracker.Entries()
                                                           .Where(entry => entry is ITrackableEntity ||
                                                                  entry.State == EntityState.Added ||
                                                                  entry.State == EntityState.Modified)
                                                           .ToArray();

            foreach (var entity in entities)
            {
                var status = entity.State == EntityState.Added ? ChangeStatus.Created : ClarifyModification(entity);
                var initiator = "admin1337";
                var track = entity.Entity as ITrackableEntity;
                track?.AddTimeStamp(initiator, status);
            }

            return base.SavingChangesAsync(eventData, result, cancellationToken);
        }

        private ChangeStatus ClarifyModification(EntityEntry entity)
        {
            var oldStatus = entity.OriginalValues.GetValue<EntityStatus>("Status");
            var newStatus = entity.CurrentValues.GetValue<EntityStatus>("Status");

            return entity.State switch
            {
                EntityState.Modified when oldStatus == EntityStatus.NotActive && newStatus == EntityStatus.Active => ChangeStatus.Reactivated,
                EntityState.Modified when oldStatus == EntityStatus.Active && newStatus == EntityStatus.NotActive => ChangeStatus.Deactivated,
                _ => ChangeStatus.Updated
            };
        }
    }
}
