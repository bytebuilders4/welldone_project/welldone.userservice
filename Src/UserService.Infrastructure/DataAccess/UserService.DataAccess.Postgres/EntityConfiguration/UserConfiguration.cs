﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Reflection.Emit;
using UserService.Entities.Entities;

namespace UserService.DataAccess.Postgres.EntityConfiguration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.OwnsMany(_ => _.TimeStamp, ownedNavigationBuilder => { ownedNavigationBuilder.ToJson(); });

            builder.OwnsOne(u => u.UserProfile);

            builder.Property(_ => _.Id)
                   .IsRequired()
                   .ValueGeneratedNever();
        }
    }
}
