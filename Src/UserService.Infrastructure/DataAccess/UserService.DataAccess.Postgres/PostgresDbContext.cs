﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using UserService.DataAccess.Postgres.EntityConfiguration;
using UserService.Entities.Entities;
using UserService.Entities.Enums;
using UserService.Interfaces.DataAccessInterfaces;

namespace UserService.DataAccess.Postgres
{
    public class PostgresDbContext : DbContext, IDbContext
    {
        public DbSet<User> Users { get; set; }

        public PostgresDbContext()
        {
            Database.EnsureCreated();
        }

        public PostgresDbContext(DbContextOptions<PostgresDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseNpgsql("Host=app_db;Port=5432;Database=userDB;Username=postgres;Password=postgres");

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
