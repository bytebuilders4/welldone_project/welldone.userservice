﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;

namespace UserService.UseCases
{
    public static class ValidationRegisterExtensions
    {
        public static MediatRServiceConfiguration AddValidationBehavior<TRequest, TResponse>(this MediatRServiceConfiguration configuration) where TRequest : notnull
        {
            return configuration.AddBehavior<IPipelineBehavior<TRequest, Result<TResponse>>, ValidationPipelineBehavior<TRequest, TResponse>>();
        }
    }
}
