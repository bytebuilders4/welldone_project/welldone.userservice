﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;
using UserService.UseCases.UserCommands.CreateUser;
using UserService.UseCases.UserCommands.UpdateUser;
using UserService.UseCases.UserQueries;
using WellDode.MassTransitModels;

namespace UserService.UseCases.UserMapping.Interface
{
    [Mapper]
    public interface IUserMapper
    {
        public UserDto MapToDto(User user);
        public ICollection<UserDto> MapToDto(ICollection<User> collection);
        public User MapToEntity(CreateUserMessage dto);
        public User MapToEntity(UpdateUserDto dto);
    }
}
