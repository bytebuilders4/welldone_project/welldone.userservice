﻿using Mapster;
using UserService.Entities.Entities;
using UserService.UseCases.UserCommands.CreateUser;
using UserService.UseCases.UserCommands.UpdateUser;
using UserService.UseCases.UserQueries;
using WellDode.MassTransitModels;

namespace UserService.UseCases.UserMapping.Configuration
{
    public class UserMappingConfigRegister : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<User, UserDto>()
                  .Map(d => d.UserProfile, s => s.UserProfile);

            config.NewConfig<UserProfileDto, UserProfile>()
                  .TwoWays();

            config.NewConfig<CreateUserMessage, User>()
                  .Map(d => d.Username, s => s.Username)
                  .Map(d => d.Id, s => s.Id)
                  .Map(d => d.UserProfile.Email, s => s.Email);

            config.NewConfig<UpdateUserDto, User>()
                  .Ignore(d => d.Status)
                  .Ignore(d => d.Rating)
                  .Map(d => d.UserProfile, s => s.UserProfile); 
        }
    }
}
