namespace UserService.UseCases.UserMapping.Interface
{
    public partial class UserMapper : UserService.UseCases.UserMapping.Interface.IUserMapper
    {
        public UserService.UseCases.UserQueries.UserDto MapToDto(UserService.Entities.Entities.User p1)
        {
            return p1 == null ? null : new UserService.UseCases.UserQueries.UserDto()
            {
                Id = p1.Id,
                Username = p1.Username,
                Rating = p1.Rating,
                Status = p1.Status,
                UserProfile = p1.UserProfile == null ? null : new UserService.UseCases.UserQueries.UserProfileDto()
                {
                    Phone = p1.UserProfile.Phone,
                    Email = p1.UserProfile.Email
                },
                RegistrationDate = p1.RegistrationDate == null ? default(System.DateTimeOffset) : (System.DateTimeOffset)p1.RegistrationDate
            };
        }
        public System.Collections.Generic.ICollection<UserService.UseCases.UserQueries.UserDto> MapToDto(System.Collections.Generic.ICollection<UserService.Entities.Entities.User> p2)
        {
            if (p2 == null)
            {
                return null;
            }
            System.Collections.Generic.ICollection<UserService.UseCases.UserQueries.UserDto> result = new System.Collections.Generic.List<UserService.UseCases.UserQueries.UserDto>(p2.Count);
            
            System.Collections.Generic.IEnumerator<UserService.Entities.Entities.User> enumerator = p2.GetEnumerator();
            
            while (enumerator.MoveNext())
            {
                UserService.Entities.Entities.User item = enumerator.Current;
                result.Add(item == null ? null : new UserService.UseCases.UserQueries.UserDto()
                {
                    Id = item.Id,
                    Username = item.Username,
                    Rating = item.Rating,
                    Status = item.Status,
                    UserProfile = item.UserProfile == null ? null : new UserService.UseCases.UserQueries.UserProfileDto()
                    {
                        Phone = item.UserProfile.Phone,
                        Email = item.UserProfile.Email
                    },
                    RegistrationDate = item.RegistrationDate == null ? default(System.DateTimeOffset) : (System.DateTimeOffset)item.RegistrationDate
                });
            }
            return result;
            
        }
        public UserService.Entities.Entities.User MapToEntity(WellDode.MassTransitModels.CreateUserMessage p3)
        {
            return p3 == null ? null : new UserService.Entities.Entities.User()
            {
                Username = p3.Username,
                UserProfile = new UserService.Entities.Entities.UserProfile() {Email = p3.Email},
                Id = p3.Id
            };
        }
        public UserService.Entities.Entities.User MapToEntity(UserService.UseCases.UserCommands.UpdateUser.UpdateUserDto p4)
        {
            return p4 == null ? null : new UserService.Entities.Entities.User()
            {
                Username = p4.Username,
                UserProfile = p4.UserProfile == null ? null : new UserService.Entities.Entities.UserProfile()
                {
                    Phone = p4.UserProfile.Phone,
                    Email = p4.UserProfile.Email
                }
            };
        }
    }
}