﻿namespace UserService.UseCases.UserQueries
{
    public class UserProfileDto
    {
        public string? Phone { get; set; }
        public string? Email { get; set; }
    }
}