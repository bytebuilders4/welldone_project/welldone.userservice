﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;
using UserService.Entities.Enums;

namespace UserService.UseCases.UserQueries
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Username { get; set; }       
        public int? Rating { get; set; }
        public EntityStatus Status { get; set; }
        public UserProfileDto UserProfile { get; set; }
        public DateTimeOffset RegistrationDate { get; set; }
    }
}
