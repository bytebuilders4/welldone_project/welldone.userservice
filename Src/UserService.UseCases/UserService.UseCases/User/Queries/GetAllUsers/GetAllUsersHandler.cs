﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using UserService.Entities.Entities;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserMapping.Interface;

namespace UserService.UseCases.UserQueries.GetAllUsers
{
    public class GetAllUsersHandler(IDbContext dbContext, IUserMapper mapper) : IRequestHandler<GetAllUsersQuery, Result<ICollection<UserDto>>>
    {
        private readonly IUserMapper _mapper = mapper;
        private readonly IDbContext _dbContext = dbContext;

        public async Task<Result<ICollection<UserDto>>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            var query = _dbContext.Users.AsNoTracking()
                                        .Where(request.Query.GetExpression());

            if (!request.Query.NoLimit)
                query = query.Skip((int)request.Query.Offset!)
                             .Take((int)request.Query.Limit!);

            var users = await query.ToListAsync();
            return (List<UserDto>)_mapper.MapToDto(users);
        }
    }
}
