﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserCommands.UpdateUser;

namespace UserService.UseCases.UserQueries.GetAllUsers
{
    public class GetAllUsersValidator : AbstractValidator<GetAllUsersQuery>
    {
        public GetAllUsersValidator(IDbContext dbContext)
        {

            RuleFor(_ => _.Query)
                .NotNull()
                .WithMessage("Query can`t be null")
                .DependentRules(() =>
                {
                    RuleFor(_ => _.Query)
                       .Must(_ => (_.NoLimit && _.Limit == null && _.Offset == null)
                               || (!_.NoLimit && _.Limit != null && _.Offset != null))
                       .WithMessage("NoLimit and pagination(limit/offset) cannot be used together, please select one of them");                    
                });

        }
    }
}