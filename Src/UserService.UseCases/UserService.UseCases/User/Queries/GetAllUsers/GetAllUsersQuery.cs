﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;
using UserService.Interfaces.DataAccessInterfaces;

namespace UserService.UseCases.UserQueries.GetAllUsers
{
    public class GetAllUsersQuery : IRequest<Result<ICollection<UserDto>>>
    {
        public required UserQuery Query { get; set; }
    }
}
