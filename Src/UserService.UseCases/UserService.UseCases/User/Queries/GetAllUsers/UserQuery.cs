﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using UserService.Entities.Entities;
using UserService.Interfaces.DataAccessInterfaces;

namespace UserService.UseCases.UserQueries.GetAllUsers
{
    public class UserQuery : IQuery<User>
    {
        public int? Offset { get; set; }
        public int? Limit { get; set; }
        public bool NoLimit { get; set; }
        public ICollection<Guid>? Ids { get; set; }
        public ICollection<string>? Names { get; set; }
        public ICollection<string>? Emails { get; set; }
        public ICollection<string>? Phones { get; set; }
        public int? MinRating { get; set; }
        public int? MaxRating { get; set; }

        public Expression<Func<User, bool>> GetExpression()
        {
            if (Ids == null && Names == null && Emails == null && Phones == null && MinRating == null && MaxRating == null)
                return user => true;

            return user => (Ids == null || Ids.Contains(user.Id))
                        && (Names == null || Names.Contains(user.Username))
                        && (Emails == null || user.UserProfile != null && user.UserProfile.Email != null && Emails.Contains(user.UserProfile.Email))
                        && (Phones == null || user.UserProfile != null && user.UserProfile.Phone != null && Phones.Contains(user.UserProfile.Phone))
                        && (MinRating == null || user.Rating > MinRating)
                        && (MaxRating == null || user.Rating < MaxRating);
        }
    }
}
