﻿using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Net;
using UserService.Entities.Entities;
using UserService.Entities.Faults;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserMapping.Interface;
using UserService.UseCases.UserQueries.GetAllUsers;
namespace UserService.UseCases.UserQueries.GetUserById
{
    public class GetUserByIdHandler(IUserMapper mapper, IDbContext dbContext) : IRequestHandler<GetUserByIdQuery, Result<UserDto>>
    {
        private readonly IUserMapper _mapper = mapper;
        private readonly IDbContext _dbContext = dbContext;

        public async Task<Result<UserDto>> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users.AsNoTracking().FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

            if (user == null)
                return new BusinessFault("Пользователь с таким идентификатором не найден", HttpStatusCode.NotFound);

            return _mapper.MapToDto(user);
        }
    }
}
