﻿using MediatR;
using UserService.Entities.Entities;

namespace UserService.UseCases.UserQueries.GetUserById
{
    public class GetUserByIdQuery : IRequest<Result<UserDto>>
    {
        public required Guid Id { get; set; }
    }
}
