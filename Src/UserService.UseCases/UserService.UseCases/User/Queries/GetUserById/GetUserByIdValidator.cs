﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserCommands.CreateUser;
using UserService.UseCases.UserQueries.GetUserById;

namespace UserService.UseCases.UserQueries.GetUserById
{
    public class GetUserByIdValidator : AbstractValidator<GetUserByIdQuery>
    {
        public GetUserByIdValidator(IDbContext dbContext)
        {
            RuleFor(_ => _.Id)
                .NotNull()
                .WithMessage("The user can`t be null");

            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithMessage("The user can`t be empty");
        }
    }
}
