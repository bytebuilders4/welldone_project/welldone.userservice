﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserCommands.ActivateUser;

namespace UserService.UseCases.UserCommands.DeactivateUser
{
    public class DeactivateUserValidator : AbstractValidator<DeactivateUserCommand>
    {
        public DeactivateUserValidator(IDbContext dbContext)
        {
            RuleFor(_ => _.Id)
                .NotNull()
                .WithMessage("The user can`t be null");

            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithMessage("The user can`t be empty");
        }
    }
}
