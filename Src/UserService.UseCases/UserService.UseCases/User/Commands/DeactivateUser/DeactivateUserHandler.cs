﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Net;
using UserService.Entities.Entities;
using UserService.Entities.Enums;
using UserService.Entities.Faults;
using UserService.Interfaces.DataAccessInterfaces;

namespace UserService.UseCases.UserCommands.DeactivateUser
{
    public class DeactivateUserHandler(IDbContext dbContext) : IRequestHandler<DeactivateUserCommand, Result<EmptyResult>>
    {
        private readonly IDbContext _dbContext = dbContext;

        public async Task<Result<EmptyResult>> Handle(DeactivateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

            if (user == null)
                return new BusinessFault("Пользователь с таким идентификатором не найден", HttpStatusCode.NotFound);

            user.ChangeStatus(EntityStatus.NotActive);

            _dbContext.Users.Update(user);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success();
        }
    }
}
