﻿using MediatR;
using System.Diagnostics.CodeAnalysis;
using UserService.Entities.Entities;
using UserService.UseCases.UserCommands.CreateUser;
using WellDode.MassTransitModels;

namespace UserService.UseCases.UserCommands.CreateUser
{
    public record CreateUserCommand : IRequest<Result<Guid>>
    {
        public required CreateUserMessage User { get; set; }
    }
}
