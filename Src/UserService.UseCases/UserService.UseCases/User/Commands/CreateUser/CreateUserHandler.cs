﻿using FluentValidation;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Net;
using UserService.Entities.Entities;
using UserService.Entities.Faults;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserMapping.Interface;

namespace UserService.UseCases.UserCommands.CreateUser
{
    public class CreateUserHandler(IUserMapper mapper, IDbContext dbContext) : IRequestHandler<CreateUserCommand, Result<Guid>>
    {
        private readonly IUserMapper _mapper = mapper;
        private readonly IDbContext _dbContext = dbContext;

        public async Task<Result<Guid>> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = _mapper.MapToEntity(request.User!);

            await _dbContext.Users.AddAsync(user, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return user.Id;
        }
    }
}
