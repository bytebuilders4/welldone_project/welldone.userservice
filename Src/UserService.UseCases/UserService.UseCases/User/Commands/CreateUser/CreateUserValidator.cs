﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;
using UserService.Interfaces.DataAccessInterfaces;

namespace UserService.UseCases.UserCommands.CreateUser
{
    public class CreateUserValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserValidator(IDbContext dbContext)
        {
            RuleFor(_ => _.User)
                .NotNull()
                .WithMessage("The user can`t be null")
                .DependentRules(() =>
                {
                    RuleFor(_ => _.User.Email)
                        .NotEmpty()
                        .WithMessage("The email can`t be empty");

                    RuleFor(_ => _.User.Email)
                       .EmailAddress()
                       .WithMessage("Incorrect format of  email");

                    RuleFor(_ => _.User.Username)
                        .NotEmpty()
                        .WithMessage("The username can`t be empty");

                    RuleFor(_ => _.User.Username)
                       .MaximumLength(30)
                       .WithMessage("The length of the name can`t be more than 30");

                    RuleFor(_ => _.User.Username)
                       .MustAsync(async (username, cancellationToken) =>
                       {
                           return !await dbContext.Users.AnyAsync(_ => _.Username.Equals(username), cancellationToken);
                       })
                       .WithMessage("The username should be unique");

                    RuleFor(_ => _.User.Id)
                        .NotEmpty()
                        .WithMessage("The Id can`t be empty");

                    RuleFor(_ => _.User.Id)
                       .MustAsync(async (Id, cancellationToken) =>
                       {
                           return !await dbContext.Users.AnyAsync(_ => _.Id.Equals(Id), cancellationToken);
                       })
                       .WithMessage("The id should be unique");
                });
        }
    }
}
