﻿using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Net;
using UserService.Entities.Entities;
using UserService.Entities.Faults;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserMapping.Interface;

namespace UserService.UseCases.UserCommands.UpdateUser
{
    public class UpdateUserHandler(IUserMapper mapper, IDbContext dbContext) : IRequestHandler<UpdateUserCommand, Result<Guid>>
    {
        private readonly IUserMapper _mapper = mapper;
        private readonly IDbContext _dbContext = dbContext;

        public async Task<Result<Guid>> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken);

            if (user == null)
                return new BusinessFault("Пользователь с таким идентификатором не найден", HttpStatusCode.NotFound);

            user.Username = request.User.Username!;
            user.UserProfile = request.User.UserProfile!;

            await _dbContext.SaveChangesAsync(cancellationToken);
            return user.Id;
        }
    }
}
