﻿using MediatR;
using System.Runtime.ConstrainedExecution;
using UserService.Entities.Entities;

namespace UserService.UseCases.UserCommands.UpdateUser
{
    public class UpdateUserCommand : IRequest<Result<Guid>>
    {
        public required Guid Id { get; set; }
        public required UpdateUserDto User { get; set; }
    }
}
