﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;
using UserService.Entities.Enums;

namespace UserService.UseCases.UserCommands.UpdateUser
{
    public class UpdateUserDto
    {
        public required string Username { get; set; }
        public int? Rating { get; private set; }
        public required UserProfile UserProfile { get; set; }
    }
}
