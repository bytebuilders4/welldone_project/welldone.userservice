﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserCommands.CreateUser;
using UserService.UseCases.UserCommands.UpdateUser;

namespace UserService.UseCases.UserCommands.UpdateUser
{
    public class UpdateUserValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserValidator(IDbContext dbContext)
        {
            RuleFor(_ => _.Id)
                .NotNull()
                .WithMessage("Id cant`be null");

            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithMessage("Id cant`be Empty");

            RuleFor(_ => _.User)
                .NotNull()
                .WithMessage("The user can`t be null")
                .DependentRules(() =>
                {
                    RuleFor(_ => _.User.Username)
                        .NotEmpty()
                        .WithMessage("The username can`t be empty");

                    RuleFor(_ => _.User.Username)
                       .MaximumLength(30)
                       .WithMessage("The length of the name can`t be more than 30");

                    RuleFor(_ => _.User.UserProfile)
                       .NotNull()
                       .WithMessage("UserProfile can`t be null")
                       .DependentRules(() =>
                       {
                           RuleFor(_ => _.User.UserProfile.Email)
                                .NotEmpty()
                                .WithMessage("The email can`t be empty");

                           RuleFor(_ => _.User.UserProfile.Email)
                              .EmailAddress()
                              .WithMessage("Incorrect format of  email");
                       });
                });

        }
    }
}