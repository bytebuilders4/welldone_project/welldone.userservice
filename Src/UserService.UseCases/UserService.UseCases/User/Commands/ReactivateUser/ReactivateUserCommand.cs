﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;

namespace UserService.UseCases.UserCommands.ActivateUser
{
    public record ReactivateUserCommand : IRequest<Result<EmptyResult>>
    {
        public required Guid Id { get; set; }
    }
}
