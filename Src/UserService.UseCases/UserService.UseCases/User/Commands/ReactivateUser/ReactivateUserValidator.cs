﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases.UserCommands.ActivateUser;
using UserService.UseCases.UserQueries.GetUserById;

namespace UserService.UseCases.UserCommands.ReactivateUser
{
    public class ReactivateUserValidator : AbstractValidator<ReactivateUserCommand>
    {
        public ReactivateUserValidator(IDbContext dbContext)
        {
            RuleFor(_ => _.Id)
                .NotNull()
                .WithMessage("The user can`t be null");

            RuleFor(_ => _.Id)
                .NotEmpty()
                .WithMessage("The user can`t be empty");
        }
    }
}
