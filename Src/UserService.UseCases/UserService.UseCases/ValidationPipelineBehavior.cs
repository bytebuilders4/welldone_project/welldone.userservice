﻿using FluentValidation;
using MediatR;
using MediatR.Pipeline;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Entities;
using UserService.Entities.Faults;

namespace UserService.UseCases
{
    public class ValidationPipelineBehavior<TRequest, TResult>(IValidator<TRequest> validator) : IPipelineBehavior<TRequest, Result<TResult>> where TRequest : notnull
    {
        private readonly IValidator<TRequest> _validator = validator;

        public async Task<Result<TResult>> Handle(TRequest request, RequestHandlerDelegate<Result<TResult>> next, CancellationToken cancellationToken)
        {
            var result = await _validator!.ValidateAsync(request, cancellationToken);

            if (!result.IsValid)
                return new ValidationFault() { Message = "Некорректные данные в запросе", ValidationErrors = result.Errors.Select(_ => _.ErrorMessage).ToList() };

            return await next();
        }
    }
}
