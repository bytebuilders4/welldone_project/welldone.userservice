﻿using MassTransit;
using MediatR;
using UserService.UseCases.UserCommands.CreateUser;
using WellDode.MassTransitModels;

namespace UserService.Web.Consumers
{
    public class CreateUserConsumer(ISender _mediator) : IConsumer<CreateUserMessage>
    {
        public async Task Consume(ConsumeContext<CreateUserMessage> context)
        {
            //TODO: tut log
            var result = await _mediator.Send(new CreateUserCommand() { User = context.Message });

            //TODO: send message to history service ? 
        }
    }
}
