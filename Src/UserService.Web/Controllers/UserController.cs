﻿using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using UserService.Entities.Faults;
using UserService.UseCases.UserCommands.ActivateUser;
using UserService.UseCases.UserCommands.CreateUser;
using UserService.UseCases.UserCommands.DeactivateUser;
using UserService.UseCases.UserCommands.UpdateUser;
using UserService.UseCases.UserQueries;
using UserService.UseCases.UserQueries.GetAllUsers;
using UserService.UseCases.UserQueries.GetUserById;
using UserService.Web.Utils;
using WellDode.MassTransitModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UserService.Web.Controllers
{

    [ApiController]
    [Route("user")]
    public class UserController(ISender sender) : ControllerBase
    {
        private readonly ISender _sender = sender;

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<UserDto>), 200)]
        [ProducesResponseType(typeof(ICollection<UserDto>), 204)]
        [ProducesResponseType(typeof(ICollection<UserDto>), 206)]
        [ProducesResponseType(typeof(ValidationFault), 400)]
        [ProducesResponseType(typeof(CommonFault), 401)]
        [ProducesResponseType(typeof(CommonFault), 403)]
        [ProducesResponseType(typeof(CommonFault), 500)]
        [Authorize]
        public async Task<IActionResult> GetAllAsync([FromQuery] UserQuery query)
        {
            var result = await _sender.Send(new GetAllUsersQuery() { Query = query });

            if (!result.IsSuccess)
                return Error(result.Error);

            return result.Data switch
            {
                _ when result.Data.Count == 0 => NoContent(),
                _ when result.Data.Count < query.Limit => new ObjectResult(result.Data) { StatusCode = (int?)HttpStatusCode.PartialContent },
                _ => Ok(result.Data)
            };
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ValidationFault), 400)]
        [ProducesResponseType(typeof(CommonFault), 401)]
        [ProducesResponseType(typeof(CommonFault), 404)]
        [ProducesResponseType(typeof(CommonFault), 403)]
        [ProducesResponseType(typeof(CommonFault), 404)]
        [ProducesResponseType(typeof(CommonFault), 500)]
        [Authorize]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            var result = await _sender.Send(new GetUserByIdQuery() { Id = id });
            return result.IsSuccess ? Ok(result.Data) : Error(result.Error);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ValidationFault), 400)]
        [ProducesResponseType(typeof(CommonFault), 401)]
        [ProducesResponseType(typeof(CommonFault), 403)]
        [ProducesResponseType(typeof(CommonFault), 404)]
        [ProducesResponseType(typeof(CommonFault), 500)]
        [Authorize]
        public async Task<IActionResult> PutAsync(Guid id, [FromBody] UpdateUserDto value)
        {
            var result = await _sender.Send(new UpdateUserCommand() { Id = id, User = value });
            return result.IsSuccess ? Ok(result.Data) : Error(result.Error);
        }

        //TODO: продумать модель ошибок
        [HttpPut("{id}/reactivate")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ValidationFault), 400)]
        [ProducesResponseType(typeof(CommonFault), 401)]
        [ProducesResponseType(typeof(CommonFault), 403)]
        [ProducesResponseType(typeof(CommonFault), 404)]
        [ProducesResponseType(typeof(CommonFault), 500)]
        [Authorize]
        public async Task<IActionResult> ReactivateUser(Guid id)
        {
            var result = await _sender.Send(new ReactivateUserCommand() { Id = id });
            return result.IsSuccess ? Ok(result.Data) : Error(result.Error);
        }

        [HttpPut("{id}/deactivate")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ValidationFault), 400)]
        [ProducesResponseType(typeof(CommonFault), 401)]
        [ProducesResponseType(typeof(CommonFault), 403)]
        [ProducesResponseType(typeof(CommonFault), 404)]
        [ProducesResponseType(typeof(CommonFault), 500)]
        [Authorize]
        public async Task<IActionResult> DeactivateUser(Guid id)
        {
            var result = await _sender.Send(new DeactivateUserCommand() { Id = id });
            return result.IsSuccess ? Ok(result.Data) : Error(result.Error);
        }

        //TODO: убрать потом.
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CreateUserMessage value)
        {
            var result = await _sender.Send(new CreateUserCommand() { User = value });

            if (result!.IsSuccess)
                return new ObjectResult(result.Data) { StatusCode = (int?)HttpStatusCode.Created };

            return Error(result.Error);
        }

        private IActionResult Error(CommonFault fault) => new ObjectResult(fault) { StatusCode = fault.HttpCode };

    }
}
