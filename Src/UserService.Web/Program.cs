using FluentValidation;
using Identity.Configuration.Extensions;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UserService.DataAccess.Postgres;
using UserService.Entities.Entities;
using UserService.Interfaces.DataAccessInterfaces;
using UserService.UseCases;
using UserService.UseCases.UserCommands.ActivateUser;
using UserService.UseCases.UserCommands.CreateUser;
using UserService.UseCases.UserCommands.DeactivateUser;
using UserService.UseCases.UserCommands.UpdateUser;
using UserService.UseCases.UserMapping.Interface;
using UserService.UseCases.UserQueries;
using UserService.UseCases.UserQueries.GetAllUsers;
using UserService.UseCases.UserQueries.GetUserById;
using UserService.Web.Consumers;
using UserService.Web.Utils;

var builder = WebApplication.CreateBuilder(args);

//builder.AddServiceDefaults();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddExceptionHandler<GlobalExceptionHandler>();
builder.Services.AddProblemDetails();
builder.Services.AddSwaggerGen();
builder.Services.AddJWTAuthentification();
builder.Services.AddCustomAuthorization();
string connection = builder.Configuration.GetConnectionString("PostgresConnection")!;

builder.Services.AddDbContext<PostgresDbContext>((serviceProvider, options) =>
{
    options.UseNpgsql(connection);
    options.AddInterceptors(serviceProvider.GetRequiredService<TrackableEntityInterceptor>());
});
builder.Services.AddSingleton<TrackableEntityInterceptor>();
builder.Services.AddScoped<IDbContext, PostgresDbContext>();
builder.Services.AddScoped<IUserMapper, UserMapper>();

builder.Services.AddMediatR(cfg =>
{
    cfg.RegisterServicesFromAssembly(typeof(CreateUserCommand).Assembly)
       .AddValidationBehavior<CreateUserCommand, Guid>()
       .AddValidationBehavior<UpdateUserCommand, Guid>()
       .AddValidationBehavior<ReactivateUserCommand, EmptyResult>()
       .AddValidationBehavior<DeactivateUserCommand, EmptyResult>()
       .AddValidationBehavior<GetUserByIdQuery, UserDto>()
       .AddValidationBehavior<GetAllUsersQuery, ICollection<UserDto>>();
});

builder.Services.AddValidatorsFromAssemblyContaining<CreateUserValidator>();

builder.Services.AddMassTransit(config =>
{
    config.SetKebabCaseEndpointNameFormatter();

    //TODO: ������� � ����� �����
    //var RabbitConfig = builder.Configuration.GetSection("RabbitMq");
    config.AddConsumer<CreateUserConsumer>();

    config.UsingRabbitMq((context, config) =>
    {

        config.Host(new Uri(builder.Configuration["RabbitMq:Host"]!), config =>
        {
            config.Username(builder.Configuration["RabbitMq:Username"]!);
            config.Password(builder.Configuration["RabbitMq:Password"]!);
        });

        config.ConfigureEndpoints(context);
    });

});

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseExceptionHandler();

//app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
