﻿using Microsoft.EntityFrameworkCore;
using UserService.Entities.Entities;

namespace UserService.Interfaces.DataAccessInterfaces
{
    public interface IDbContext
    {
        DbSet<User> Users { get; set; }
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
