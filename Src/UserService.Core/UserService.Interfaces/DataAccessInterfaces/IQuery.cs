﻿using System.Linq.Expressions;

namespace UserService.Interfaces.DataAccessInterfaces
{
    public interface IQuery<T> where T : class
    {
        public bool NoLimit { get; set; }
        public int? Offset { get; set; }
        public int? Limit { get; set; }
        public Expression<Func<T, bool>> GetExpression();

    }
}