﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Faults;

namespace UserService.Entities.Entities
{
    public class Result<T>
    {
        public bool IsSuccess { get; }
        public T Data { get; set; }
        public CommonFault Error { get; set; }

        internal Result(T data)
        {
            Data = data;
            IsSuccess = true;
        }

        internal Result(CommonFault error)
        {
            Error = error;
            IsSuccess = false;
        }

        public static implicit operator Result<T>(T data) => new(data);

        public static implicit operator Result<T>(CommonFault error) => new(error);
    }

    public class Result
    {
        public static Result<EmptyResult> Success() => new(new EmptyResult());
    }

    public class EmptyResult { }
}
