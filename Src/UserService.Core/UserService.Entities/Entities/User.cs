﻿using System.Numerics;
using UserService.Entities.Enums;

namespace UserService.Entities.Entities;

public class User() : EntityBase<Guid>
{
    public required string Username { get; set; }
    public int? Rating { get; private set; }
    public required UserProfile UserProfile { get; set; }
    public DateTimeOffset? RegistrationDate { get => TimeStamp.FirstOrDefault()?.ChangeTime; }

    public void ChangeStatus(EntityStatus status) => Status = status;
}
