﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Enums;

namespace UserService.Entities.Entities;

public abstract class EntityBase<T> : ITrackableEntity where T : notnull
{
    public T Id { get; set; } = default!;
    public IList<TimeStamp> TimeStamp { get; } = [];

    public EntityStatus Status { get; protected set; }

    public void AddTimeStamp(string initiator, ChangeStatus change)
    {
        //TODO: validation
        var timeStamp = new TimeStamp()
        {
            Initiator = initiator,
            Change = change,
            ChangeTime = DateTime.UtcNow
        };

        TimeStamp.Add(timeStamp);
    }
}


