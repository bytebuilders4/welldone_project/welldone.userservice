﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Enums;

namespace UserService.Entities.Entities
{
    public interface ITrackableEntity
    {
        public IList<TimeStamp> TimeStamp { get; }
        public void AddTimeStamp(string initiator, ChangeStatus change);
    }
}
