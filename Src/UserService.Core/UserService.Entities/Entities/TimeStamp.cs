﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Entities.Enums;

namespace UserService.Entities.Entities
{
    public class TimeStamp
    {
        public required string Initiator { get; set; }
        public required DateTimeOffset ChangeTime { get; set; }
        public required ChangeStatus Change { get; set; }
    }
}
