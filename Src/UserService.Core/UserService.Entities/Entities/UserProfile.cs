﻿namespace UserService.Entities.Entities
{
    public class UserProfile
    {
        public string? Phone { get; set; }
        public required string Email { get; set; }
    }
}