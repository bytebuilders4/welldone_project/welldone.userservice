﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserService.Entities.Enums
{
    public enum ChangeStatus
    {
        Created,
        Updated,
        Deactivated,
        Reactivated
    }
}
