﻿namespace UserService.Entities.Faults
{
    public abstract class CommonFault
    {
        public string Message { get; set; }
        public int HttpCode { get; set; }
        public string Trace { get; set; }
        public DateTimeOffset Date { get; private set; }

        protected CommonFault()
        {
            Date = DateTimeOffset.UtcNow;
        }
    }
}