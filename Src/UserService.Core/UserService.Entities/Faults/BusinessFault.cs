﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UserService.Entities.Faults
{
    public class BusinessFault : CommonFault
    {       

        public BusinessFault(string message, HttpStatusCode httpCode = HttpStatusCode.InternalServerError)
        {
            Message = message;
            HttpCode = (int)httpCode;
        }

        public BusinessFault()
        {
            HttpCode = (int)HttpStatusCode.InternalServerError;
        }
    }
}
