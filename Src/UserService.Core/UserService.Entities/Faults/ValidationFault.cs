﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UserService.Entities.Faults
{
    public class ValidationFault : CommonFault
    {
        public List<string> ValidationErrors { get; set; }

        public ValidationFault()
        {
            HttpCode = (int)HttpStatusCode.BadRequest;
        }
    }
}

